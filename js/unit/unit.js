"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const router = express.Router();
const ensureAuthenticated_1 = require("../functions/ensureAuthenticated");
const model_1 = require("./model");
const model_2 = require("../group/model");
const model_3 = require("../topic/model");
const sanitize_1 = require("../functions/sanitize");
router.post('/', ensureAuthenticated_1.default, (req, res) => __awaiter(this, void 0, void 0, function* () {
    if (!sanitize_1.default.verifyWord(req.body.groupId))
        return res.status(400).send("Invalid data");
    if (!sanitize_1.default.verifyString(req.body.name))
        return res.status(400).send("Invalid data");
    if (!sanitize_1.default.verifyString(req.body.number))
        return res.status(400).send("Invalid data");
    if (req.body.description && !sanitize_1.default.verifyString(req.body.description))
        return res.status(400).send("Invalid data");
    try {
        let group = yield model_2.Group.getById(req.body.groupId);
        if (!group)
            throw { code: 404, msg: "Group not found" };
        if (group.users.indexOf(req.auth.id) === -1)
            throw { code: 404, msg: "User not in the group" };
        let units = yield model_1.Unit.getByGroupId(group._id);
        let num = units.filter(x => +(x.number) === +(req.body.number));
        if (num.length > 0)
            throw { code: 400, msg: "Invalid unit number" };
        let newUnit = new model_1.Unit({
            name: req.body.name,
            group: group._id,
            description: req.body.description,
            number: req.body.number,
            createdBy: req.auth.id
        });
        newUnit.save((err, unit) => {
            if (err)
                throw { code: 500, msg: "Unable to add unit" };
            res.status(201).json(unit);
        });
    }
    catch (err) {
        console.error(`[${new Date}, POST /unit/]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
}));
router.patch('/', ensureAuthenticated_1.default, (req, res) => __awaiter(this, void 0, void 0, function* () {
    if (!sanitize_1.default.verifyWord(req.body.id))
        return res.status(400).send("Invalid data");
    if (req.body.name && !sanitize_1.default.verifyString(req.body.name))
        return res.status(400).send("Invalid data");
    if (req.body.description && !sanitize_1.default.verifyString(req.body.description))
        return res.status(400).send("Invalid data");
    try {
        let unit = yield model_1.Unit.getById(req.body.id);
        if (!unit)
            throw { code: 404, msg: "Unit not found" };
        let group = yield model_2.Group.getById(unit.group);
        if (!group)
            throw { code: 404, msg: "Group does not exist anymore" };
        if (group.users.indexOf(req.auth.id) === -1)
            throw { code: 404, msg: "User not in the group" };
        model_1.Unit.findOneAndUpdate({ _id: req.body.id }, { $set: { "name": req.body.name, "description": req.body.description } }, (err, unit) => {
            if (err) {
                console.log(`[${new Date}, PATCH /unit/]`, err);
                return res.sendStatus(500);
            }
            res.json(unit);
        });
    }
    catch (err) {
        console.error(`[${new Date}, PATCH /unit/]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
}));
router.get('/:id/topics', ensureAuthenticated_1.default, (req, res) => __awaiter(this, void 0, void 0, function* () {
    if (!sanitize_1.default.verifyWord(req.params.id))
        return res.status(400).send("Invalid data");
    try {
        let unit = yield model_1.Unit.getById(req.params.id);
        if (!unit)
            throw { code: 404, msg: "Unit not found" };
        let group = yield model_2.Group.getById(unit.group);
        if (!group)
            throw { code: 404, msg: "Group not found" };
        if (group.users.indexOf(req.auth.id) === -1)
            throw { code: 404, msg: "User not in the group" };
        model_3.Topic.find({ unit: unit._id }, (err, topics) => {
            if (err)
                throw err;
            res.json(topics);
        });
    }
    catch (err) {
        console.error(`[${new Date}, GET /unit/:id/topics]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
}));
exports.default = router;
