'use strict';
const sanitize = {
  verifyWord: (a:string) => {
    if(!a || a==="") return false;
    if(a.length>1000000) return false;
    const regex = /[^a-z0-9]/i;
    return !regex.test(a);
  },
  verifyToken: (a:string) => {
    if(!a || a==="") return false;
    if(a.length>1000000) return false;
    const noDots = /\./;
    const dots = /\.[^\.]*\./;
    const twoDots = /\.\./;
    const overDots = /\.[^\.]*\.[^\.]*\./;
    const regex = /[^-_.a-z0-9]/i;
    if(regex.test(a)) return false;
    if(!noDots.test(a)) return false;
    if(twoDots.test(a)) return false;
    if(!dots.test(a)) return false;
    return !overDots.test(a);
  },
  verifyString: (a:string | string[]) => {
    if(!a || a==="") return false;
    if(typeof(a)!=="string") return false;
    if(a.length>100000) return false;
    const regex = /[\$\{\};]|(http:\/\/|https:\/\/)/i;
    return !regex.test(a);
  },
  verifyBool: (value:boolean) => {
    if(typeof value!== 'boolean') return false;
    return true;
  },
  verifyNumber: (n:number | string) => {
    if(typeof(n)==="number") return true;
    if(n.length>100000) return false;
    const regex = /[^-.0-9]/i;
    return !regex.test(n);
  },
  verifyEmail: (email:string) => {
    if(!email || email==="") return false;
    if(email.length>1000) return false;
    const doubleAt = /@[^@]*@/;
    const at = /@/;
    const dot = /\./;
    const regex = /[^-@._a-zA-Z0-9]/;
    if(regex.test(email)) return false;
    if(!at.test(email)) return false;
    if(!dot.test(email)) return false;
    return !doubleAt.test(email);
  },
  verifyName: (a:string) => {
    if(!a || a==="") return false;
    if(a.length>200) return false;
    const regex = /[^-a-zA-Z'ąćęłńóśżź\ ]/i;
    return !regex.test(a);
  }
}

export default sanitize;
