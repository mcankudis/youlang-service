"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const db_1 = require("../db");
const getById = async (id) => {
    try {
        const topic = await exports.Topic.findOne({ _id: id });
        if (!topic)
            throw { code: 404, msg: "Topic not found" };
        return topic;
    }
    catch (err) {
        console.error(`[${new Date()}, topic/model/getById]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const getByUnitId = async (id) => {
    try {
        const topics = await exports.Topic.find({ unit: id });
        return topics;
    }
    catch (err) {
        console.error(`[${new Date()}, group/model/getByTopicId]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const getByGroupId = async (id) => {
    try {
        const topics = await exports.Topic.find({ group: id });
        return topics;
    }
    catch (err) {
        console.error(`[${new Date()}, group/model/getByGroupId]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const topicSchema = new db_1.default.Schema({
    name: { type: String, required: true },
    group: { type: String, required: true },
    unit: { type: String, required: true },
    number: { type: Number, required: true },
    description: { type: String },
    createdBy: { type: String, required: true }
}, { timestamps: true });
const topic = db_1.default.model("Topic", topicSchema);
exports.Topic = Object.assign(topic, {
    getById: getById,
    getByGroupId: getByGroupId,
    getByUnitId: getByUnitId
});
