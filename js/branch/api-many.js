"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const router = express.Router();
const ensureAuthenticated_1 = require("../functions/ensureAuthenticated");
const model_1 = require("./model");
const model_2 = require("../group/model");
const sanitize_1 = require("../functions/sanitize");
router.get('/:groupId', ensureAuthenticated_1.default, (req, res) => __awaiter(this, void 0, void 0, function* () {
    let chosenGroup = req.params.groupId;
    if (!sanitize_1.default.verifyWord(chosenGroup))
        return res.sendStatus(401);
    try {
        let group = yield model_2.Group.getById(chosenGroup);
        if (!group)
            throw { code: 404, msg: "Group not found" };
        if (group.users.indexOf(req.auth.id) === -1)
            throw { code: 404, msg: "User not in the group" };
        let branches = yield model_1.Branch.getByGroupId(group._id);
        res.json(branches);
    }
    catch (err) {
        console.error(`[${new Date}, GET /branches/:groupId]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
}));
exports.default = router;
