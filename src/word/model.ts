"use strict";
import db from '../db';

export type WordDocument = db.Document & {
	word: string;
  explaination: string;
  unit: string;
  group: string;
  topic: string;
  createdAt: string;
  createdBy: string;
  updatedAt: string;
  getById: getByIdFunction;
  getByGroupId: getManyFunction;
  getByTopicId: getManyFunction;
  getByUnitId: getManyFunction;
};

type getByIdFunction = (id: string) => Promise<WordDocument>;
type getManyFunction = (id: string) => Promise<Array<WordDocument>>;

const getById: getByIdFunction = async(id) => {
	try {
		const word = await Word.findOne({_id: id});
    if(!word) throw {code: 404, msg: "Word not found"};
    return word;
	}
	catch(err) {
		console.error(`[${new Date()}, word/model/getById]`, err);
		if(err.msg) throw err;
		throw {code: 500, msg: "An error occured"};
	}
}
const getByUnitId: getManyFunction = async(id) => {
	try {
		const word = await Word.find({unit: id});
    	return word;
	}
	catch(err) {
		console.error(`[${new Date()}, group/model/getByWordId]`, err);
		if(err.msg) throw err;
		throw {code: 500, msg: "An error occured"};
	}
}
const getByGroupId: getManyFunction = async(id) => {
	try {
		const word = await Word.find({group: id});
    	return word;
	}
	catch(err) {
		console.error(`[${new Date()}, group/model/getByGroupId]`, err);
		if(err.msg) throw err;
		throw {code: 500, msg: "An error occured"};
	}
}
const getByTopicId: getManyFunction = async(id) => {
	try {
		const word = await Word.find({topic: id});
    	return word;
	}
	catch(err) {
		console.error(`[${new Date()}, group/model/getByTopicId]`, err);
		if(err.msg) throw err;
		throw {code: 500, msg: "An error occured"};
	}
}

const wordSchema = new db.Schema({
  word:         {type: String, required: true},
  explaination: {type: String, required: true},
  group:        {type: String, required: true},
  unit:					{type: String},
  topic:				{type: String},
  createdBy:    {type: String, required: true}
}, { timestamps: true });

const word = db.model<WordDocument>("Word", wordSchema);
export const Word = Object.assign(word, {
  getById: getById,
  getByGroupId: getByGroupId,
	getByUnitId: getByUnitId,
	getByTopicId: getByTopicId
})
