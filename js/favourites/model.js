"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const db_1 = require("../db");
const addToFav = async (wordId, userId) => {
    try {
        let res = exports.Favourites.updateOne({ user_id: userId }, { $push: { list: wordId } });
        console.log(res);
        return true;
    }
    catch (err) {
        console.error('[favourites/model/addToFav]', err);
        throw err;
    }
};
const removeFromFav = async (wordId, userId) => {
    try {
        let res = exports.Favourites.updateOne({ user_id: userId }, { $pull: { list: wordId } });
        console.log(res);
        return true;
    }
    catch (err) {
        console.error('[favourites/model/removeFromFav]', err);
        throw err;
    }
};
const create = async (userId) => {
    try {
        let newFavs = new exports.Favourites({ user_id: userId });
        let favs = await newFavs.save();
        console.log(favs);
        return favs;
    }
    catch (err) {
        console.error('[favourites/model/create]', err);
        throw err;
    }
};
const getByUserId = async (id) => {
    try {
        const favs = await exports.Favourites.findOne({ user_id: id });
        if (!favs)
            throw { code: 404, msg: "List not found" };
        return favs;
    }
    catch (err) {
        console.error(`[${new Date()}, favourites/model/getByUserId]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const schema = new db_1.default.Schema({
    user_id: { type: String, required: true, index: true, unique: true },
    list: { type: Array, default: [] },
}, { timestamps: true });
const favs = db_1.default.model("Favourites", schema);
exports.Favourites = Object.assign(favs, {
    addWord: addToFav,
    removeWord: removeFromFav,
    getByUserId: getByUserId
});
