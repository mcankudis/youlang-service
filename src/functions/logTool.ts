import sanitize from './sanitize';
import * as jwt from 'jsonwebtoken';
import { Req, Res, Auth } from "../types";

export const logRequest = (req: Req, res: Res, next: Function) => {
  console.log('New request');
  console.log('path', req.path);
  console.log('headers', req.headers);
  console.log('body', req.body);
  next();
}
