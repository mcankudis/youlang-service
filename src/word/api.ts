"use strict";
import * as express from 'express';
const router = express.Router();
import ensureAuthenticated from '../functions/ensureAuthenticated';
import { Word } from './model';
import { Group } from '../group/model';
import { Topic } from '../topic/model';
import { Unit } from '../unit/model';
import { Req, Res } from "../types";
import sanitize from '../functions/sanitize';

router.post('/', ensureAuthenticated, async(req: Req, res: Res) => {
  try {
    if(!sanitize.verifyWord(req.body.groupId)) throw {code: 400, msg:"Invalid data"};
    if(!sanitize.verifyString(req.body.word)) throw {code: 400, msg:"Invalid data"};
    if(!sanitize.verifyString(req.body.explaination)) throw {code: 400, msg:"Invalid data"};
    let group = await Group.getById(req.body.groupId);
    if(group.users.indexOf(req.auth.id)==-1) throw {code: 404, msg:"User not in the group"};
    let newWord = new Word({
      word: req.body.word,
      explaination: req.body.explaination,
      group: group._id,
      createdBy: req.auth.username
    });
    if(req.body.unit) {
      if(!sanitize.verifyWord(req.body.unit)) throw {code: 400, msg:"Invalid data"};
      let unit = await Unit.getById(req.body.unit);
      newWord.unit = unit._id;
    }
    if(req.body.topic) {
      if(!sanitize.verifyWord(req.body.topic)) throw {code: 400, msg:"Invalid data"};
      let topic = await Topic.getById(req.body.topic);
      newWord.topic = topic._id;
      if(!req.body.unit) {
        let unit = await Unit.getById(topic.unit);
        newWord.unit = unit._id;
      }
    }
    let word = await newWord.save();
    if(!word) throw {code: 500, msg:"Unable to add word"};
    res.status(200).json(word);
  }
  catch(err) {
    console.error(`[${new Date}, POST /word]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})

router.patch('/:id', ensureAuthenticated, async(req: Req, res: Res) => {
  try {
    if(!sanitize.verifyWord(req.params.id)) throw {code: 400, msg:"Invalid word id"};
    if(req.body.word && !sanitize.verifyString(req.body.word)) throw {code: 400, msg:"Invalid word"};
    if(req.body.explaination && !sanitize.verifyString(req.body.explaination)) throw {code: 400, msg:"Invalid explaination"};
    if(req.body.topic && !sanitize.verifyString(req.body.topic)) throw {code: 400, msg:"Invalid topic"};
    if(req.body.unit && !sanitize.verifyString(req.body.unit)) throw {code: 400, msg:"Invalid unit"};
    let word = await Word.getById(req.params.id);
    let group = await Group.getById(word.group);
    if(group.users.indexOf(req.auth.id)==-1) throw {code: 404, msg:"User not in the group"};
    let newWord = req.body.word || word.word;
    let newExpl = req.body.explaination || word.explaination;
    let newTopic = req.body.topic || word.topic;
    let newUnit = req.body.unit || word.unit;
    Word.findOneAndUpdate({_id: word._id},
    {$set:{"word": newWord, "explaination": newExpl, "topic": newTopic, "unit": newUnit}},
    {new: true},
    (err, wrd) => {
      if(err) throw {code: 500, msg:"Unable to update word"};
      console.log(err)
      console.log(wrd)
      res.json(wrd);
    })
  }
  catch(err) {
    console.error(`[${new Date}, PATCH /word]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})

router.delete('/:id', ensureAuthenticated, async(req: Req, res: Res) => {
  try {
    if(!sanitize.verifyWord(req.params.id)) throw {code: 400, msg:"Invalid data"};
    let word = await Word.getById(req.params.id);
    let group = await Group.getById(word.group);
    if(group.users.indexOf(req.auth.id)==-1) throw {code: 404, msg:"User not in the group"};
    Word.deleteOne({_id: word._id}, (result) => {
      console.log(result);
      res.sendStatus(200);
    })
  }
  catch(err) {
    console.error(`[${new Date}, DELETE /word]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})

export default router;
