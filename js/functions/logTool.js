"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logRequest = (req, res, next) => {
    console.log('New request');
    console.log('path', req.path);
    console.log('headers', req.headers);
    console.log('body', req.body);
    next();
};
