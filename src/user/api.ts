"use strict";
import * as express from 'express';
const router = express.Router();
import ensureAuthenticated from '../functions/ensureAuthenticated';
import { Unit } from '../unit/model';
import { Group } from '../group/model';
import { Topic } from '../topic/model';
import { Word } from '../word/model';
import { YouLangUser } from './model';
import { User } from './userservice';
import { Req, Res } from "../types";
import sanitize from '../functions/sanitize';

// GET ALL GROUPS THE USER IS IN
router.get('/groups', ensureAuthenticated, async(req: Req, res: Res) => {
  try {
    let groups = await Group.getByUserId(req.auth.id);
    groups = JSON.parse(JSON.stringify(groups));
    if(!groups) return res.json([]);
    let promises = [];
    groups.forEach(g => promises.push(Unit.getByGroupId(g._id)));
    let units = await Promise.all(promises);
    units = JSON.parse(JSON.stringify(units));
    promises = [];
    let unitsFlat = units.flat();
    unitsFlat.forEach(u => promises.push(Topic.getByUnitId(u._id)));
    let topics = await Promise.all(promises);
    promises = [];
    unitsFlat.forEach((u, i) => u.topics = topics[i]);
    groups.forEach((g) => {
      g.units = unitsFlat.filter(u => u.group===g._id);
      promises.push(Word.getByGroupId(g._id));
    });
    let words = await Promise.all(promises);
    words = JSON.parse(JSON.stringify(words));
    let topicsFlat = topics.flat();
    let topicsForWords = topicsFlat.reduce((start, t) => {start[t._id] = {_id: t._id, name: t.name}; return start}, {});
    let unitsForWords = unitsFlat.reduce((start, u) => {start[u._id] = {_id: u._id, name: u.name}; return start}, {});
    groups.forEach((g, i) => {
      words[i].forEach(w => {
        if(w.topic) w.topic = topicsForWords[w.topic];
        if(w.unit) w.unit = unitsForWords[w.unit];
      })
      g.words = words[i];
    });
    let users = {};
    groups.forEach(g => {
      g.users.forEach(p => {
        if(typeof p === 'string') users[p] = p;
      })
      g.admins.forEach(p => {
        if(typeof p === 'string') users[p] = p;
      })
    });
    let ids = Object.values(users);
    promises = ids.map((id:string) => {
      return new Promise(async(resolve, reject) => {
        try {
          let user = await User.getById(id);
          if(!user) throw {msg: 'User not found'};
          users[id] = user;
          resolve(true);
        }
        catch(err) {
          console.error(`${new Date()}, [GET routes/groups/promises]`, err);
          reject(null);
        }
      });
    });
    Promise.all(promises)
    .then(function(players) {
      groups = groups.map(g=> {
        g.users = g.users.map(id => {
          if(typeof id !== 'string') return id;
          return {
            _id: users[id]._id,
            username: users[id].username
          }
        });
        g.admins = g.admins.map(id => {
          if(typeof id !== 'string') return id;
          return {
            _id: users[id]._id,
            username: users[id].username
          }
        });
        return g;
      })
      return res.json(groups);
    })
    .catch(console.error)
  }
  catch(err) {
    console.error(`[${new Date()}, GET /user/groups]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    return res.sendStatus(500);
  }
})

// router.get('/words/favorites', ensureAuthenticated, async(req: Req, res: Res) => {
//   try {
//     let user = await YouLangUser.getById(req.auth.id);
//     if(!user) user = await YouLangUser.add(req.auth.id, req.auth.username);
//     res.json(user.favoriteWords);
//   }
//   catch(err) {
//     console.error(`[${new Date}, GET /user/words/favorites]`, err);
//     if(err.msg) return res.status(err.code).send(err.msg);
//     res.status(500).send("Unknown error");
//   }
// })

// router.post('/words/favorites', ensureAuthenticated, async(req: Req, res: Res) => {
//   try {
//     if(!sanitize.verifyWord(req.body.wordId)) throw {code: 400, msg:"Invalid data"};
//     let user = await YouLangUser.getByIdOrCreate(req.auth.id, req.auth.username);
//     let fav = new Set(user.favoriteWords);
//     if(fav.has(req.body.wordId)) fav.delete(req.body.wordId);
//     else fav.add(req.body.wordId);
//     let favs = Array.from(fav);
//     let response = await YouLangUser.updateOne({_id: req.auth.id}, {$set: {"favoriteWords": favs}});
//     if(!response.ok) throw {code: 500, msg: "Unable to mark/unmark as favorite"};
//     res.sendStatus(200);
//   }
//   catch(err) {
//     console.error(`[${new Date}, POST /user/words/favorites]`, err);
//     if(err.msg) return res.status(err.code).send(err.msg);
//     res.status(500).send("Unknown error");
//   }
// })

export default router;
