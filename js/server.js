'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = require("dotenv");
dotenv.config();
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const helmet = require("helmet");
// Security headers
app.use(helmet());
// Body parse
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// These are the routes
const api_1 = require("./group/api");
const api_2 = require("./cardbox/api");
const api_3 = require("./topic/api");
const api_4 = require("./unit/api");
const api_5 = require("./user/api");
const api_6 = require("./word/api");
app.use('/group', api_1.default);
app.use('/cardbox', api_2.default);
app.use('/topic', api_3.default);
app.use('/unit', api_4.default);
app.use('/user', api_5.default);
app.use('/word', api_6.default);
const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(new Date, `Hi, listening on port ${port}`);
});
