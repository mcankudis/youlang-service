'use strict';
import * as dotenv from 'dotenv';
dotenv.config();
import * as express from 'express';
const app = express();
import * as bodyParser from 'body-parser';
import * as helmet from 'helmet';

// Security headers
app.use(helmet());

// Body parse
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

// These are the routes
import groupRoute from './group/api';
import cardboxRoute from './cardbox/api';
import topicRoute from './topic/api';
import unitRoute from './unit/api';
import userRoute from './user/api';
import wordRoute from './word/api';
app.use('/group', groupRoute);
app.use('/cardbox', cardboxRoute);
app.use('/topic', topicRoute);
app.use('/unit', unitRoute);
app.use('/user', userRoute);
app.use('/word', wordRoute);

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(new Date, `Hi, listening on port ${port}`);
})
