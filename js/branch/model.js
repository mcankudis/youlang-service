"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const db_1 = require("../db");
const getById = (id) => __awaiter(this, void 0, void 0, function* () {
    try {
        const chat = yield exports.Branch.findOne({ _id: id });
        if (!chat)
            throw { code: 404, msg: "Branch not found" };
        return chat;
    }
    catch (err) {
        console.error(`[${new Date()}, branch/model/getById]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
});
const getByGroupId = (id) => __awaiter(this, void 0, void 0, function* () {
    try {
        const chat = yield exports.Branch.find({ group: id });
        if (!chat)
            throw { code: 404, msg: "Branches not found" };
        return chat;
    }
    catch (err) {
        console.error(`[${new Date()}, branch/model/getByGroupId]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
});
const branchSchema = new db_1.default.Schema({
    name: { type: String, required: true },
    group: { type: String, required: true },
    number: { type: Number, required: true },
    description: { type: String },
    createdBy: { type: String, required: true }
}, { timestamps: true });
const branch = db_1.default.model("Branch", branchSchema);
exports.Branch = Object.assign(branch, {
    getById: getById,
    getByGroupId: getByGroupId
});
