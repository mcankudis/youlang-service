"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r = require("request-promise-native");
const sanitize_1 = require("../functions/sanitize");
const baseUrl = process.env.USERSERVICE_ADDRESS || "http://localhost:3101";
exports.User = {
    getById: async (id) => {
        try {
            if (!sanitize_1.default.verifyWord(id))
                throw { code: 401, msg: "Invalid data - illegal signs" };
            let user = await r(`${baseUrl}/user/id/${id}`);
            if (!user)
                throw { code: 404, msg: "User not found" };
            return JSON.parse(user);
        }
        catch (err) {
            console.error('[user/userservice/User/getById]', err);
            if (err.msg)
                throw err;
            throw { code: 500, msg: "Unknown error" };
        }
    },
    getByUsername: async (username) => {
        try {
            if (!sanitize_1.default.verifyString(username))
                throw { code: 401, msg: "Invalid data - illegal signs" };
            let user = await r(`${baseUrl}/user/username/${username}`);
            if (!user)
                throw { code: 404, msg: "User not found" };
            return JSON.parse(user);
        }
        catch (err) {
            console.error('[user/userservice/User/getByUsername]', err);
            if (err.msg)
                throw err;
            throw { code: 500, msg: "Unknown error" };
        }
    }
};
