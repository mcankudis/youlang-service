"use strict";
import db from '../db';

export type GroupDocument = db.Document & { 
	name: string;
	description: string;
	users: Array<string | object>;
	admins: Array<string | object>;
  createdAt: string;
  updatedAt: string;
  createdBy: string;
  addAdmin: addUserFunction;
  addUser: addUserFunction;
  getById: getByIdFunction;
  getByUserId: getManyFunction;
  removeUser: removeUserFunction;
  removeAdmin: removeUserFunction;
  delete: deleteFunction;
  units?: Array<object>;
  words?: any;
};

type getByIdFunction = (id: string) => Promise<GroupDocument>;
type getManyFunction = (id: string) => Promise<Array<GroupDocument>>;
type addUserFunction = (groupId: string, userId: string) => Promise<object>;
type removeUserFunction = (groupId: string, userId: string) => Promise<object>;
type deleteFunction = (groupId: string) => Promise<object>;

const addAdmin: addUserFunction = async(groupId, userId) => {
  try {
    let result = await Group.updateOne({_id: groupId}, {$addToSet: {admins: [userId]}});
    console.log(`${new Date}, group/model/addAdmin`, result);
    if(result.nModified === 0) throw {code: 404, msg: "Group not found"};
    return {success: true};
  }
  catch(err) {
    console.error(`[${new Date}, group/model/addAdmin]`, err);
    if(err.msg) throw err;
    throw {code: 500, msg: "An error occured"};
  }
}
const addUser: addUserFunction = async(groupId, userId) => {
  try {
    let result = await Group.updateOne({_id: groupId}, {$addToSet: {users: [userId]}});
    console.log(`${new Date}, group/model/addUser`, result);
    if(result.nModified === 0) throw {code: 404, msg: "Group not found"};
    return {success: true};
  }
  catch(err) {
    console.error(`[${new Date}, group/model/addUser]`, err);
    if(err.msg) throw err;
    throw {code: 500, msg: "An error occured"};
  }
}
const deleteGroup: deleteFunction = async(groupId) => {
  try {
    let result = await Group.deleteOne({_id: groupId});
    console.log(`${new Date}, group/model/deleteGroup`, result);
    if(result.ok === 0) throw {code: 404, msg: "Group not found"};
    return {success: true};
  }
  catch(err) {
    console.error(`[${new Date}, group/model/deleteGroup]`, err);
    if(err.msg) throw err;
    throw {code: 500, msg: "An error occured"};
  }
}
const getById: getByIdFunction = async(id) => {
	try {
		const group = await Group.findOne({_id: id});
    if(!group) throw {code: 404, msg: "Group not found"};
    return group;
	}
	catch(err) {
		console.error(`[${new Date()}, group/model/getById]`, err);
		if(err.msg) throw err;
		throw {code: 500, msg: "An error occured"};
	}
}
const getByUserId: getManyFunction = async(id) => {
	try {
		const group = await Group.find({users: id});
    return group;
	}
	catch(err) {
		console.error(`${new Date()}, [group/model/getByUserId]`, err);
		if(err.msg) throw err;
		throw {code: 500, msg: "An error occured"};
	}
}
const removeAdmin: removeUserFunction = async(groupId, userId) => {
  try {
    let result = await Group.updateOne({_id: groupId}, {$pull: {admins: userId}})
    console.log(`${new Date}, group/model/removeAdmin`, result);
    if(result.nModified === 0) throw {code: 404, msg: "Group not found"};
    return {success: true};
  }
  catch(err) {
    console.error(`[${new Date}, group/model/removeAdmin]`, err);
    if(err.msg) throw err;
    throw {code: 500, msg: "An error occured"};
  }
}
const removeUser: removeUserFunction = async(groupId, userId) => {
  try {
    let result = await Group.updateOne({_id: groupId}, {$pull: {users: userId}})
    console.log(`${new Date}, group/model/removeUser`, result);
    if(result.nModified === 0) throw {code: 404, msg: "Group not found"};
    return {success: true};
  }
  catch(err) {
    console.error(`[${new Date}, group/model/removeUser]`, err);
    if(err.msg) throw err;
    throw {code: 500, msg: "An error occured"};
  }
}

const groupSchema = new db.Schema({
    name:         {type: String, required: true},
    description:  {type: String},
    users:        {type: Array, required: true},
    admins:       {type: Array, required: true},
    createdBy:    {type: String, required: true},
}, { timestamps: true });

const group = db.model<GroupDocument>("Group", groupSchema);
export const Group = Object.assign(group, {
  addAdmin: addAdmin,
  addUser: addUser,
  getById: getById,
  getByUserId: getByUserId,
  removeUser: removeUser,
  removeAdmin: removeAdmin,
  delete: deleteGroup
})
