"use strict";
import * as express from 'express';
const router = express.Router();
import ensureAuthenticated from '../functions/ensureAuthenticated';
import { Unit } from '../unit/model';
import { Group } from './model';
import { Topic } from '../topic/model';
import { Word } from '../word/model';
import sanitize from '../functions/sanitize';
import { Req, Res } from "../types";
import { User } from "../user/userservice";

// GET GROUP BY ID
router.get('/:id', ensureAuthenticated, async(req: Req, res: Res) => {
  if(!sanitize.verifyWord(req.params.id)) return res.status(400).send("Invalid data");
  // verifyIdFunction????
  try {
    let group = await Group.getById(req.params.id);
    if(!group) throw {code: 404, msg: "Group not found"};
    res.json(group);
  }
  catch(err) {
    console.error(`[${new Date}, GET /group/:id]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})

// CREATE GROUP
router.post('/', ensureAuthenticated, (req: Req, res: Res) => {
  if(!sanitize.verifyString(req.body.name)) return res.status(400).send("Invalid data");
  if(!sanitize.verifyString(req.body.description)) return res.status(400).send("Invalid data");
  let newGroup = new Group({
    name: req.body.name,
    description: req.body.description,
    users: [req.auth.id],
    admins: [req.auth.id],
    createdBy: req.auth.id
  });
  newGroup.save((err, group) => {
    if(err) {
      console.error(`[${new Date()}, POST /group/]`, err);
      return res.sendStatus(503);
    }
    group.users = [{_id: req.auth.id, username: req.auth.username}];
    group.admins = [{_id: req.auth.id, username: req.auth.username}];
    res.status(201).json(group);
  })
})

// ADD A USER TO THE GROUP
router.post('/user/', ensureAuthenticated, async(req: Req, res: Res) => {
  let groupId = req.body.group,
  usernameToAdd = req.body.user;
  try {
    if(!sanitize.verifyWord(groupId)) throw {code: 400, msg: "Invalid group"};
    if(!sanitize.verifyString(usernameToAdd)) throw {code: 400, msg: "Invalid user"};
    let userToAdd = await User.getByUsername(usernameToAdd);
    if(!userToAdd) throw {code: 404, msg: "User not found"};
    let group = await Group.getById(groupId);
    if(!group) throw {code: 404, msg: "Group not found"};
    if(group.admins.indexOf(req.auth.id)===-1) throw {code: 401, msg: "Admin permission missing"};
    if(group.users.indexOf(userToAdd._id)!==-1) throw {code: 400, msg: "User already in the group"};
    await Group.addUser(group._id, userToAdd._id);
    res.status(200).json({_id: userToAdd._id, username: userToAdd.username});
  }
  catch(err) {
    console.error(`[${new Date}, POST /group/addUser]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})

// REMOVE A USER FROM GROUP
router.delete('/user', ensureAuthenticated, async(req: Req, res: Res) => {
  let groupId = req.body.group,
  userIdToRemove = req.body.user;
  try {
    if(!sanitize.verifyWord(groupId)) throw {code: 400, msg: "Invalid group"};
    if(!sanitize.verifyWord(userIdToRemove)) throw {code: 400, msg: "Invalid user"};
    let group = await Group.getById(groupId);
    if(!group) throw {code: 404, msg: "Group not found"};
    if(userIdToRemove!=req.auth.id) {
      if(group.admins.indexOf(req.auth.id)===-1)
        throw {code: 401, msg: "Admin permission missing"};
      if(group.createdBy === userIdToRemove)
        throw {code: 400, msg: "Creator can't be removed"};
    }
    if(group.users.length === 1) await Group.delete(group._id);
    else await Group.removeUser(group._id, userIdToRemove);
    res.sendStatus(200);
  }
  catch(err) {
    console.error(`[${new Date}, POST /group/removeUser]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})

// ADD A USER TO GROUPS' ADMINS
router.post('/admin/', ensureAuthenticated, async(req: Req, res: Res) => {
  let groupId = req.body.group,
  usernameToAdd = req.body.user;
  if(!sanitize.verifyWord(groupId)) return res.status(400).send("Invalid data");
  if(!sanitize.verifyString(usernameToAdd)) return res.status(400).send("Invalid data");
  try {
    let userToAdd = await User.getByUsername(usernameToAdd);
    if(!userToAdd) throw {code: 404, msg: "User not found"};
    let group = await Group.getById(groupId);
    if(!group) throw {code: 404, msg: "Group not found"};
    if(group.admins.indexOf(req.auth.id)===-1) throw {code: 401, msg: "Admin permission missing"};
    if(group.admins.indexOf(userToAdd._id)!==-1) throw {code: 400, msg: "User is already an admin"};
    await Group.addAdmin(group._id, userToAdd._id);
    res.json({newAdmin: {username: userToAdd.username, _id: userToAdd._id}});
  }
  catch(err) {
    console.error(`[${new Date}, POST /group/addUser]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})

// REMOVE A USER FROM GROUPS' ADMINS
router.delete('/admin/', ensureAuthenticated, async(req: Req, res: Res) => {
  let groupId = req.body.group,
  userIdToRemove = req.body.user;
  if(!sanitize.verifyWord(groupId)) return res.status(400).send("Invalid data");
  if(!sanitize.verifyWord(userIdToRemove)) return res.status(400).send("Invalid data");
  try {
    let group = await Group.getById(groupId);
    if(!group) throw {code: 404, msg: "Group not found"};
    if(group.admins.indexOf(userIdToRemove)===-1)
      throw {code: 400, msg: "This user is not an admin"}
    if(group.admins.indexOf(req.auth.id)===-1)
      throw {code: 401, msg: "Admin permission missing"};
    if(group.createdBy === userIdToRemove)
      throw {code: 400, msg: "Creator can't be demoted"};
    await Group.removeAdmin(group._id, userIdToRemove);
    res.sendStatus(200);
  }
  catch(err) {
    console.error(`[${new Date}, POST /group/removeAdmin]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})

router.get('/:id/units', ensureAuthenticated, async(req: Req, res: Res) => {
  let chosenGroup = req.params.id;
  if(!sanitize.verifyWord(chosenGroup)) return res.status(400).send("Invalid data");;
  try {
    let group = await Group.getById(chosenGroup);
    if(!group) throw {code: 404, msg: "Group not found"};
    if(group.users.indexOf(req.auth.id)===-1)
      throw {code: 404, msg: "User not in the group"};
    let units = await Unit.getByGroupId(group._id);
    res.json(units);
  }
  catch(err) {
    console.error(`[${new Date}, GET /group/:id/units]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})

router.get('/:id/topics', ensureAuthenticated, async(req: Req, res: Res) => {
  if(!sanitize.verifyWord(req.params.id)) return res.status(400).send("Invalid data");
  try {
    let group = await Group.getById(req.params.id);
    if(!group) throw {code: 404, msg: "Group not found"};
    if(group.users.indexOf(req.auth.id)===-1)
      throw {code: 404, msg: "User not in the group"};
    Topic.find({group: group._id}, (err, topics) => {
      if(err) return res.sendStatus(404);
      res.json(topics)
    })
  }
  catch(err) {
    console.error(`[${new Date}, GET /group/:id/topics]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})

router.get('/:id/words/', ensureAuthenticated, async(req: Req, res: Res) => {
  try {
    if(!sanitize.verifyWord(req.params.id)) throw {code: 400, msg: "Invalid data"};
    let group = await Group.getById(req.params.id);
    if(group.users.indexOf(req.auth.id)==-1) throw {code: 404, msg: "User not in the group"};
    let words = await Word.getByGroupId(group._id);
    res.json(words);
  }
  catch(err) {
    console.error(`[${new Date}, GET /group/:id/words]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})



export default router;
