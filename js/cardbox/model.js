"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const db_1 = require("../db");
const getById = async (id) => {
    try {
        const box = await exports.Cardbox.findOne({ _id: id });
        if (!box)
            throw { code: 404, msg: "Cardbox not found" };
        return box;
    }
    catch (err) {
        console.error(`[${new Date()}, cardbox/model/getById]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const cardboxSchema = new db_1.default.Schema({
    name: { type: String, required: true },
    boxes: [
        { type: Array, default: [] },
        { type: Array, default: [] },
        { type: Array, default: [] },
        { type: Array, default: [] },
        { type: Array, default: [] },
        { type: Array, default: [] },
        { type: Array, default: [] }
    ],
    createdBy: { type: String, required: true }
}, { timestamps: true });
const cardbox = db_1.default.model("Cardbox", cardboxSchema);
exports.Cardbox = Object.assign(cardbox, {
    getById: getById
});
