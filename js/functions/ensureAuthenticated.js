"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sanitize_1 = require("./sanitize");
const jwt = require("jsonwebtoken");
const ensureAuthenticated = (req, res, next) => {
    if (!req.headers['x-auth'] || !sanitize_1.default.verifyString(req.headers['x-auth']))
        return res.status(403).send("Missing credentials");
    jwt.verify(req.headers['x-auth'], process.env.SECRET, function (err, token) {
        if (err) {
            console.error(`[${new Date}, functions/ensureAuthenticated]`, err);
            return res.status(403).send("Permission denied");
        }
        req.auth = token;
        return next();
    });
};
exports.default = ensureAuthenticated;
