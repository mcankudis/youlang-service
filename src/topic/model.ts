"use strict";
import db from '../db';

export type TopicDocument = db.Document & {
	name: string;
  description: string;
  createdAt: string;
  createdBy: string;
  unit: string;
  group: string;
  number: number;
  updatedAt: string;
  getById: getByIdFunction;
  getByGroupId: getByIdFunction;
  getByUnitId: getByIdFunction;
};

type getByIdFunction = (id: string) => Promise<TopicDocument>;
type getManyFunction = (id: string) => Promise<Array<TopicDocument>>;

const getById: getByIdFunction = async(id) => {
	try {
		const topic = await Topic.findOne({_id: id});
    	if(!topic) throw {code: 404, msg: "Topic not found"};
    	return topic;
	}
	catch(err) {
		console.error(`[${new Date()}, topic/model/getById]`, err);
		if(err.msg) throw err;
		throw {code: 500, msg: "An error occured"};
	}
}
const getByUnitId: getManyFunction = async(id) => {
	try {
		const topics = await Topic.find({unit: id});
	    return topics;
	}
	catch(err) {
		console.error(`[${new Date()}, group/model/getByTopicId]`, err);
		if(err.msg) throw err;
		throw {code: 500, msg: "An error occured"};
	}
}
const getByGroupId: getManyFunction = async(id) => {
	try {
		const topics = await Topic.find({group: id});
    	return topics;
	}
	catch(err) {
		console.error(`[${new Date()}, group/model/getByGroupId]`, err);
		if(err.msg) throw err;
		throw {code: 500, msg: "An error occured"};
	}
}

const topicSchema = new db.Schema({
  name:         {type: String, required: true},
  group:        {type: String, required: true},
  unit:					{type: String, required: true},
  number:       {type: Number, required: true},
  description:  {type: String},
  createdBy:    {type: String, required: true}
}, { timestamps: true });

const topic = db.model<TopicDocument>("Topic", topicSchema);
export const Topic = Object.assign(topic, {
  getById: getById,
  getByGroupId: getByGroupId,
	getByUnitId: getByUnitId
})
