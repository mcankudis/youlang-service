"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const router = express.Router();
const ensureAuthenticated_1 = require("../functions/ensureAuthenticated");
const model_1 = require("../group/model");
// GET ALL GROUPS THE USER IS IN
router.get('/groups', ensureAuthenticated_1.default, (req, res) => {
    model_1.Group.find({ users: req.auth.id }, (err, groups) => {
        if (err) {
            console.error(`[${new Date()}, GET /user/groups]`, err);
            return res.sendStatus(500);
        }
        if (!groups)
            return res.sendStatus(404);
        res.json(groups);
    });
});
exports.default = router;
