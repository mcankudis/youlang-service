"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const r = require("request-promise-native");
const sanitize_1 = require("../functions/sanitize");
const baseUrl = process.env.USERSERVICE_ADDRESS || "http://localhost:3101";
exports.User = {
    getById: (id) => __awaiter(this, void 0, void 0, function* () {
        try {
            if (!sanitize_1.default.verifyWord(id))
                throw { code: 401, msg: "Invalid data - illegal signs" };
            let user = yield r(`${baseUrl}/user/id/${id}`);
            if (!user)
                throw { code: 404, msg: "User not found" };
            return JSON.parse(user);
        }
        catch (err) {
            console.error('[userservice/User/findById]', err);
            if (err.msg)
                throw err;
            throw { code: 500, msg: "Unknown error" };
        }
    })
};
