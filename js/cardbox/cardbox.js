"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const router = express.Router();
const model_1 = require("./model");
const sanitize_1 = require("../functions/sanitize");
router.get('/:id', (req, res) => __awaiter(this, void 0, void 0, function* () {
    if (!sanitize_1.default.verifyWord(req.params.id))
        return res.status(400).send("Invalid data");
    // verifyIdFunction????
    try {
        let cardbox = yield model_1.Cardbox.getById(req.params.id);
        if (!cardbox)
            throw { code: 404, msg: "Cardbox not found" };
        // words from ids here
        res.json(cardbox);
    }
    catch (err) {
        console.error(`[${new Date}, GET /cardbox/:id]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
}));
router.post('/', (req, res) => __awaiter(this, void 0, void 0, function* () {
    if (!sanitize_1.default.verifyWord(req.body.name))
        return res.status(400).send("Invalid data");
    try {
        let newCardbox = new model_1.Cardbox({
            name: req.body.name,
            createdBy: req.auth.id
        });
        let box = yield newCardbox.save();
        if (!box)
            throw { code: 500, msg: "Unable to create cardbox" };
        res.status(201).json(box);
    }
    catch (err) {
        console.error(`[${new Date}, POST /cardbox/]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
}));
exports.default = router;
