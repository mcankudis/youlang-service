import * as express from 'express';

export interface Auth {
  id: string;
  username: string;
}

export interface Req extends express.Request {
  auth?: Auth;
  headers: {
    'x-auth': string;
  };
}

export interface Res extends express.Response {
}

export type TokenAuth = {
  _id?: string;
  username?: string;
  random?: string;
};
