"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const db_1 = require("../db");
const getById = async (id) => {
    try {
        const word = await exports.Word.findOne({ _id: id });
        if (!word)
            throw { code: 404, msg: "Word not found" };
        return word;
    }
    catch (err) {
        console.error(`[${new Date()}, word/model/getById]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const getByUnitId = async (id) => {
    try {
        const word = await exports.Word.find({ unit: id });
        return word;
    }
    catch (err) {
        console.error(`[${new Date()}, group/model/getByWordId]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const getByGroupId = async (id) => {
    try {
        const word = await exports.Word.find({ group: id });
        return word;
    }
    catch (err) {
        console.error(`[${new Date()}, group/model/getByGroupId]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const getByTopicId = async (id) => {
    try {
        const word = await exports.Word.find({ topic: id });
        return word;
    }
    catch (err) {
        console.error(`[${new Date()}, group/model/getByTopicId]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const wordSchema = new db_1.default.Schema({
    word: { type: String, required: true },
    explaination: { type: String, required: true },
    group: { type: String, required: true },
    unit: { type: String },
    topic: { type: String },
    createdBy: { type: String, required: true }
}, { timestamps: true });
const word = db_1.default.model("Word", wordSchema);
exports.Word = Object.assign(word, {
    getById: getById,
    getByGroupId: getByGroupId,
    getByUnitId: getByUnitId,
    getByTopicId: getByTopicId
});
