"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const router = express.Router();
const ensureAuthenticated_1 = require("../functions/ensureAuthenticated");
const model_1 = require("./model");
const sanitize_1 = require("../functions/sanitize");
router.get('/:id', ensureAuthenticated_1.default, async (req, res) => {
    if (!sanitize_1.default.verifyWord(req.params.id))
        return res.status(400).send("Invalid data");
    // verifyIdFunction????
    try {
        let cardbox = await model_1.Cardbox.getById(req.params.id);
        if (!cardbox)
            throw { code: 404, msg: "Cardbox not found" };
        // words from ids here
        res.json(cardbox);
    }
    catch (err) {
        console.error(`[${new Date}, GET /cardbox/:id]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
});
router.post('/', ensureAuthenticated_1.default, async (req, res) => {
    if (!sanitize_1.default.verifyWord(req.body.name))
        return res.status(400).send("Invalid data");
    try {
        let newCardbox = new model_1.Cardbox({
            name: req.body.name,
            createdBy: req.auth.id
        });
        let box = await newCardbox.save();
        if (!box)
            throw { code: 500, msg: "Unable to create cardbox" };
        res.status(201).json(box);
    }
    catch (err) {
        console.error(`[${new Date}, POST /cardbox/]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
});
exports.default = router;
