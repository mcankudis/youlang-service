"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const router = express.Router();
const ensureAuthenticated_1 = require("../functions/ensureAuthenticated");
const model_1 = require("./model");
const model_2 = require("../group/model");
const model_3 = require("../topic/model");
const model_4 = require("../unit/model");
const sanitize_1 = require("../functions/sanitize");
router.post('/', ensureAuthenticated_1.default, async (req, res) => {
    try {
        if (!sanitize_1.default.verifyWord(req.body.groupId))
            throw { code: 400, msg: "Invalid data" };
        if (!sanitize_1.default.verifyString(req.body.word))
            throw { code: 400, msg: "Invalid data" };
        if (!sanitize_1.default.verifyString(req.body.explaination))
            throw { code: 400, msg: "Invalid data" };
        let group = await model_2.Group.getById(req.body.groupId);
        if (group.users.indexOf(req.auth.id) == -1)
            throw { code: 404, msg: "User not in the group" };
        let newWord = new model_1.Word({
            word: req.body.word,
            explaination: req.body.explaination,
            group: group._id,
            createdBy: req.auth.username
        });
        if (req.body.unit) {
            if (!sanitize_1.default.verifyWord(req.body.unit))
                throw { code: 400, msg: "Invalid data" };
            let unit = await model_4.Unit.getById(req.body.unit);
            newWord.unit = unit._id;
        }
        if (req.body.topic) {
            if (!sanitize_1.default.verifyWord(req.body.topic))
                throw { code: 400, msg: "Invalid data" };
            let topic = await model_3.Topic.getById(req.body.topic);
            newWord.topic = topic._id;
            if (!req.body.unit) {
                let unit = await model_4.Unit.getById(topic.unit);
                newWord.unit = unit._id;
            }
        }
        let word = await newWord.save();
        if (!word)
            throw { code: 500, msg: "Unable to add word" };
        res.status(200).json(word);
    }
    catch (err) {
        console.error(`[${new Date}, POST /word]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
});
router.patch('/:id', ensureAuthenticated_1.default, async (req, res) => {
    try {
        if (!sanitize_1.default.verifyWord(req.params.id))
            throw { code: 400, msg: "Invalid word id" };
        if (req.body.word && !sanitize_1.default.verifyString(req.body.word))
            throw { code: 400, msg: "Invalid word" };
        if (req.body.explaination && !sanitize_1.default.verifyString(req.body.explaination))
            throw { code: 400, msg: "Invalid explaination" };
        if (req.body.topic && !sanitize_1.default.verifyString(req.body.topic))
            throw { code: 400, msg: "Invalid topic" };
        if (req.body.unit && !sanitize_1.default.verifyString(req.body.unit))
            throw { code: 400, msg: "Invalid unit" };
        let word = await model_1.Word.getById(req.params.id);
        let group = await model_2.Group.getById(word.group);
        if (group.users.indexOf(req.auth.id) == -1)
            throw { code: 404, msg: "User not in the group" };
        let newWord = req.body.word || word.word;
        let newExpl = req.body.explaination || word.explaination;
        let newTopic = req.body.topic || word.topic;
        let newUnit = req.body.unit || word.unit;
        model_1.Word.findOneAndUpdate({ _id: word._id }, { $set: { "word": newWord, "explaination": newExpl, "topic": newTopic, "unit": newUnit } }, { new: true }, (err, wrd) => {
            if (err)
                throw { code: 500, msg: "Unable to update word" };
            console.log(err);
            console.log(wrd);
            res.json(wrd);
        });
    }
    catch (err) {
        console.error(`[${new Date}, PATCH /word]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
});
router.delete('/:id', ensureAuthenticated_1.default, async (req, res) => {
    try {
        if (!sanitize_1.default.verifyWord(req.params.id))
            throw { code: 400, msg: "Invalid data" };
        let word = await model_1.Word.getById(req.params.id);
        let group = await model_2.Group.getById(word.group);
        if (group.users.indexOf(req.auth.id) == -1)
            throw { code: 404, msg: "User not in the group" };
        model_1.Word.deleteOne({ _id: word._id }, (result) => {
            console.log(result);
            res.sendStatus(200);
        });
    }
    catch (err) {
        console.error(`[${new Date}, DELETE /word]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
});
exports.default = router;
