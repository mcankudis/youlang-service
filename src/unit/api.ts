"use strict";
import * as express from 'express';
const router = express.Router();
import ensureAuthenticated from '../functions/ensureAuthenticated';
import { Unit } from './model';
import { Group } from '../group/model';
import { Topic } from '../topic/model';
import { Req, Res } from "../types";
import sanitize from '../functions/sanitize';
import { logRequest } from '../functions/logTool';

router.post('/', logRequest, ensureAuthenticated, async(req: Req, res: Res) => {
  if(!sanitize.verifyWord(req.body.groupId)) return res.status(400).send("Invalid group");
  if(!sanitize.verifyString(req.body.name)) return res.status(400).send("Invalid unit name");
  if(!sanitize.verifyNumber(req.body.number)) return res.status(400).send("Invalid unit number");
  if(req.body.description && !sanitize.verifyString(req.body.description))
    return res.status(400).send("Invalid data");
  try {
    let group = await Group.getById(req.body.groupId);
    if(!group) throw {code: 404, msg: "Group not found"};
    if(group.users.indexOf(req.auth.id)===-1)
      throw {code: 404, msg: "User not in the group"};
    let units = await Unit.getByGroupId(group._id);
    let num = units.filter(x=> +(x.number) === +(req.body.number));
    if(num.length>0) throw {code: 400, msg: "Invalid unit number"};
    let newUnit = new Unit({
      name: req.body.name,
      group: group._id,
      description: req.body.description,
      number: req.body.number,
      createdBy: req.auth.id
    });
    newUnit.save((err, unit) => {
      if(err) throw {code: 500, msg: "Unable to add unit"}
      res.status(201).json(unit);
    })
  }
  catch(err) {
    console.error(`[${new Date}, POST /unit/]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})

router.patch('/:id', ensureAuthenticated, async(req: Req, res: Res) => {
  if(!sanitize.verifyWord(req.params.id)) return res.status(400).send("Invalid data");
  if(req.body.name && !sanitize.verifyString(req.body.name)) return res.status(400).send("Invalid data");
  if(req.body.description && !sanitize.verifyString(req.body.description)) return res.status(400).send("Invalid data");
  try {
    let unit = await Unit.getById(req.params.id);
    if(!unit) throw {code: 404, msg: "Unit not found"};
    let group = await Group.getById(unit.group);
    if(!group) throw {code: 404, msg: "Group does not exist anymore"};
    if(group.users.indexOf(req.auth.id)===-1)
      throw {code: 404, msg: "User not in the group"};
    Unit.findOneAndUpdate({_id: unit._id},
    {$set:{"name": req.body.name, "description": req.body.description}}, (err, unit) => {
      if(err) {
        console.log(`[${new Date}, PATCH /unit/]`, err);
        return res.sendStatus(500);
      }
      res.json(unit);
    })
  }
  catch(err) {
    console.error(`[${new Date}, PATCH /unit/]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})

router.get('/:id/topics', ensureAuthenticated, async(req: Req, res: Res) => {
  if(!sanitize.verifyWord(req.params.id)) return res.status(400).send("Invalid data");
  try {
    let unit = await Unit.getById(req.params.id);
    if(!unit) throw {code: 404, msg: "Unit not found"};
    let group = await Group.getById(unit.group);
    if(!group) throw {code: 404, msg: "Group not found"};
    if(group.users.indexOf(req.auth.id)===-1)
      throw {code: 404, msg: "User not in the group"};
    Topic.find({unit: unit._id}, (err, topics) => {
      if(err) throw err;
      res.json(topics)
    })
  }
  catch(err) {
    console.error(`[${new Date}, GET /unit/:id/topics]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})

export default router;
