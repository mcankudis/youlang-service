"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const db_1 = require("../db");
const addUser = async (id, username) => {
    try {
        let newUser = new exports.YouLangUser({
            _id: id,
            username: username,
            settings: {},
            favoriteWords: []
        });
        const user = await newUser.save();
        return user;
    }
    catch (err) {
        console.error('[models/user/addUser]', err);
        throw err;
    }
};
const getById = async (id) => {
    try {
        const user = await exports.YouLangUser.findOne({ _id: id });
        if (!user)
            throw { code: 404, msg: "User not found" };
        return user;
    }
    catch (err) {
        console.error(`[${new Date()}, user/model/getById]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const getByIdOrCreate = async (id, username) => {
    try {
        let user = await exports.YouLangUser.findOne({ _id: id });
        if (!user)
            user = await exports.YouLangUser.add(id, username);
        return user;
    }
    catch (err) {
        console.error(`[${new Date()}, user/model/getByIdOrCreate]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const userSchema = new db_1.default.Schema({
    username: { type: String, required: true },
    favoriteWords: { type: Array },
    settings: { type: Object }
}, { timestamps: true });
const user = db_1.default.model("User", userSchema);
exports.YouLangUser = Object.assign(user, {
    add: addUser,
    getById: getById,
    getByIdOrCreate: getByIdOrCreate
});
