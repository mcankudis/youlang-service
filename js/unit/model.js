"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const db_1 = require("../db");
const getById = async (id) => {
    try {
        const unit = await exports.Unit.findOne({ _id: id });
        if (!unit)
            throw { code: 404, msg: "Unit not found" };
        return unit;
    }
    catch (err) {
        console.error(`[${new Date()}, unit/model/getById]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const getByGroupId = async (id) => {
    try {
        const unit = await exports.Unit.find({ group: id });
        if (!unit)
            throw { code: 404, msg: "Unites not found" };
        return unit;
    }
    catch (err) {
        console.error(`[${new Date()}, unit/model/getByGroupId]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const unitSchema = new db_1.default.Schema({
    name: { type: String, required: true },
    group: { type: String, required: true },
    number: { type: Number, required: true },
    description: { type: String },
    createdBy: { type: String, required: true }
}, { timestamps: true });
const unit = db_1.default.model("Unit", unitSchema);
exports.Unit = Object.assign(unit, {
    getById: getById,
    getByGroupId: getByGroupId
});
