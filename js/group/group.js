"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const router = express.Router();
const ensureAuthenticated_1 = require("../functions/ensureAuthenticated");
const model_1 = require("../unit/model");
const model_2 = require("./model");
const model_3 = require("../topic/model");
const sanitize_1 = require("../functions/sanitize");
const userservice_1 = require("../user/userservice");
// GET GROUP BY ID
router.get('/:id', ensureAuthenticated_1.default, (req, res) => __awaiter(this, void 0, void 0, function* () {
    if (!sanitize_1.default.verifyWord(req.params.id))
        return res.status(400).send("Invalid data");
    // verifyIdFunction????
    try {
        let group = yield model_2.Group.getById(req.params.id);
        if (!group)
            throw { code: 404, msg: "Group not found" };
        res.json(group);
    }
    catch (err) {
        console.error(`[${new Date}, GET /group/:id]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
}));
// CREATE GROUP
router.post('/', ensureAuthenticated_1.default, (req, res) => {
    if (!sanitize_1.default.verifyString(req.body.name))
        return res.status(400).send("Invalid data");
    if (!sanitize_1.default.verifyString(req.body.description))
        return res.status(400).send("Invalid data");
    let newGroup = new model_2.Group({
        name: req.body.name,
        description: req.body.description,
        users: [req.auth.id],
        admins: [req.auth.id],
        createdBy: req.auth.id
    });
    newGroup.save((err, group) => {
        if (err) {
            console.error(`[${new Date()}, POST /group/]`, err);
            return res.sendStatus(503);
        }
        res.status(201).json(group);
    });
});
// ADD A USER TO THE GROUP
router.post('/user/', ensureAuthenticated_1.default, (req, res) => __awaiter(this, void 0, void 0, function* () {
    let groupId = req.body.group, userIdToAdd = req.body.user;
    if (!sanitize_1.default.verifyWord(groupId))
        return res.sendStatus(401);
    if (!sanitize_1.default.verifyWord(userIdToAdd))
        return res.sendStatus(401);
    try {
        let userToAdd = yield userservice_1.User.getById(userIdToAdd);
        if (!userToAdd)
            throw { code: 404, msg: "User not found" };
        let group = yield model_2.Group.getById(groupId);
        if (!group)
            throw { code: 404, msg: "Group not found" };
        if (group.admins.indexOf(req.auth.id) === -1)
            throw { code: 401, msg: "Admin permission missing" };
        if (group.users.indexOf(userToAdd._id) !== -1)
            throw { code: 400, msg: "User already in the group" };
        yield model_2.Group.addUser(group._id, userToAdd._id);
        res.status(200).json({ _id: userToAdd._id, username: userToAdd.username });
    }
    catch (err) {
        console.error(`[${new Date}, POST /group/addUser]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
}));
// REMOVE A USER FROM GROUP
router.delete('/user/', ensureAuthenticated_1.default, (req, res) => __awaiter(this, void 0, void 0, function* () {
    let groupId = req.body.group, userIdToRemove = req.body.user;
    if (!sanitize_1.default.verifyWord(groupId))
        return res.sendStatus(401);
    if (!sanitize_1.default.verifyWord(userIdToRemove))
        return res.sendStatus(401);
    try {
        let group = yield model_2.Group.getById(groupId);
        if (!group)
            throw { code: 404, msg: "Group not found" };
        if (userIdToRemove != req.auth.id) {
            if (group.admins.indexOf(req.auth.id) === -1)
                throw { code: 401, msg: "Admin permission missing" };
            if (group.createdBy === userIdToRemove)
                throw { code: 401, msg: "Creator can't be removed" };
        }
        yield model_2.Group.removeUser(group._id, userIdToRemove);
        res.sendStatus(200);
    }
    catch (err) {
        console.error(`[${new Date}, POST /group/removeUser]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
}));
// ADD A USER TO GROUPS' ADMINS
router.post('/admin/', ensureAuthenticated_1.default, (req, res) => __awaiter(this, void 0, void 0, function* () {
    let groupId = req.body.group, userIdToAdd = req.body.user;
    if (!sanitize_1.default.verifyWord(groupId))
        return res.sendStatus(401);
    if (!sanitize_1.default.verifyWord(userIdToAdd))
        return res.sendStatus(401);
    try {
        let userToAdd = yield userservice_1.User.getById(userIdToAdd);
        if (!userToAdd)
            throw { code: 404, msg: "User not found" };
        let group = yield model_2.Group.getById(groupId);
        if (!group)
            throw { code: 404, msg: "Group not found" };
        if (group.admins.indexOf(req.auth.id) === -1)
            throw { code: 401, msg: "Admin permission missing" };
        if (group.admins.indexOf(userToAdd._id) !== -1)
            throw { code: 400, msg: "User is already an admin" };
        yield model_2.Group.addAdmin(group._id, userToAdd._id);
        res.json({ newAdmin: { username: userToAdd.username, _id: userToAdd._id } });
    }
    catch (err) {
        console.error(`[${new Date}, POST /group/addUser]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
}));
// REMOVE A USER FROM GROUPS' ADMINS
router.delete('/admin/', ensureAuthenticated_1.default, (req, res) => __awaiter(this, void 0, void 0, function* () {
    let groupId = req.body.group, userIdToRemove = req.body.user;
    if (!sanitize_1.default.verifyWord(groupId))
        return res.sendStatus(401);
    if (!sanitize_1.default.verifyWord(userIdToRemove))
        return res.sendStatus(401);
    try {
        let group = yield model_2.Group.getById(groupId);
        if (!group)
            throw { code: 404, msg: "Group not found" };
        if (group.admins.indexOf(userIdToRemove) === -1)
            throw { code: 400, msg: "This user is not an admin" };
        if (group.admins.indexOf(req.auth.id) === -1)
            throw { code: 401, msg: "Admin permission missing" };
        if (group.createdBy === userIdToRemove)
            throw { code: 401, msg: "Creator can't be demoted" };
        yield model_2.Group.removeAdmin(group._id, userIdToRemove);
        res.sendStatus(200);
    }
    catch (err) {
        console.error(`[${new Date}, POST /group/removeAdmin]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
}));
router.get('/:id/units', ensureAuthenticated_1.default, (req, res) => __awaiter(this, void 0, void 0, function* () {
    let chosenGroup = req.params.id;
    if (!sanitize_1.default.verifyWord(chosenGroup))
        return res.status(400).send("Invalid data");
    ;
    try {
        let group = yield model_2.Group.getById(chosenGroup);
        if (!group)
            throw { code: 404, msg: "Group not found" };
        if (group.users.indexOf(req.auth.id) === -1)
            throw { code: 404, msg: "User not in the group" };
        let units = yield model_1.Unit.getByGroupId(group._id);
        res.json(units);
    }
    catch (err) {
        console.error(`[${new Date}, GET /group/:id/units]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
}));
router.get('/:id/topics', ensureAuthenticated_1.default, (req, res) => __awaiter(this, void 0, void 0, function* () {
    if (!sanitize_1.default.verifyWord(req.params.id))
        return res.sendStatus(401);
    try {
        let group = yield model_2.Group.getById(req.params.id);
        if (!group)
            throw { code: 404, msg: "Group not found" };
        if (group.users.indexOf(req.auth.id) === -1)
            throw { code: 404, msg: "User not in the group" };
        model_3.Topic.find({ group: group._id }, (err, topics) => {
            if (err)
                return res.sendStatus(404);
            res.json(topics);
        });
    }
    catch (err) {
        console.error(`[${new Date}, GET /group/:id/topics]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.status(500).send("Unknown error");
    }
}));
exports.default = router;
