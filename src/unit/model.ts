"use strict";
import db from '../db';

export type UnitDocument = db.Document & {
	name: string;
  description: string;
	group: string;
  createdAt: string;
  createdBy: string;
  number: number;
  updatedAt: string;
  getById: getByIdFunction;
  getByGroupId: getByIdFunction;
};

type getByIdFunction = (id: string) => Promise<UnitDocument>;
type getByGroupIdFunction = (id: string) => Promise<UnitDocument[]>;

const getById: getByIdFunction = async(id) => {
	try {
		const unit = await Unit.findOne({_id: id});
    if(!unit) throw {code: 404, msg: "Unit not found"};
    return unit;
	}
	catch(err) {
		console.error(`[${new Date()}, unit/model/getById]`, err);
		if(err.msg) throw err;
		throw {code: 500, msg: "An error occured"};
	}
}
const getByGroupId: getByGroupIdFunction = async(id) => {
	try {
		const unit = await Unit.find({group: id});
    if(!unit) throw {code: 404, msg: "Unites not found"};
    return unit;
	}
	catch(err) {
		console.error(`[${new Date()}, unit/model/getByGroupId]`, err);
		if(err.msg) throw err;
		throw {code: 500, msg: "An error occured"};
	}
}

const unitSchema = new db.Schema({
  name:         {type: String, required: true},
  group:        {type: String, required: true},
  number:       {type: Number, required: true},
  description:  {type: String},
  createdBy:    {type: String, required: true}
}, { timestamps: true });

const unit = db.model<UnitDocument>("Unit", unitSchema);
export const Unit = Object.assign(unit, {
  getById: getById,
  getByGroupId: getByGroupId
})
