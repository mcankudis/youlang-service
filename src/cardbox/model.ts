"use strict";
import db from '../db';

export type CardboxDocument = db.Document & {
	name: string;
  boxes: [
    Array<string | object>,
    Array<string | object>,
    Array<string | object>,
    Array<string | object>,
    Array<string | object>,
    Array<string | object>,
    Array<string | object>
  ]
  createdAt: string;
  createdBy: string;
  updatedAt: string;
  getById: getByIdFunction;
};

type getByIdFunction = (id: string) => Promise<CardboxDocument>;

const getById: getByIdFunction = async(id) => {
	try {
		const box = await Cardbox.findOne({_id: id});
    if(!box) throw {code: 404, msg: "Cardbox not found"};
    return box;
	}
	catch(err) {
		console.error(`[${new Date()}, cardbox/model/getById]`, err);
		if(err.msg) throw err;
		throw {code: 500, msg: "An error occured"};
	}
}

const cardboxSchema = new db.Schema({
  name:         {type: String, required: true},
  boxes:        [
    {type: Array, default: []},
    {type: Array, default: []},
    {type: Array, default: []},
    {type: Array, default: []},
    {type: Array, default: []},
    {type: Array, default: []},
    {type: Array, default: []}
  ],
  createdBy:    {type: String, required: true}
}, { timestamps: true });

const cardbox = db.model<CardboxDocument>("Cardbox", cardboxSchema);
export const Cardbox = Object.assign(cardbox, {
  getById: getById
})
