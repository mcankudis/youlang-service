"use strict";
import { User } from "../../js/userservice/api";
import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
const expect = chai.expect;
chai.use(chaiAsPromised);

describe('findById', () => {
  it('valid userId', async() => {
    let id = "5d06b58f3a315e34c00a7b6b";
    return expect(User.getById(id)).to.eventually.have.property('username');
  })
})
//
// describe('Login function - basic test', () => {
//   it('Returns an object with property username', async() => {
//     const User = {
//       getByUsername: async(username) => {
//         return { username }
//       },
//       comparePassword: () => true
//     }
//     return expect(login('test', 'empty', User)).to.eventually.have.property('username');
//   })
//   it('Calls userDependency.getByUsername with proper username', async() => {
//     const User = {
//       getByUsername: async(username) => {
//         expect(username).to.equal('test')
//         return {
//           password: 'something'
//         }
//       },
//       comparePassword: async(password, password2) => true
//     }
//     await login('test', 'empty', User);
//   })
//   it('Calls userDependency.comparePassword with proper password', async() => {
//     const User = {
//       getByUsername: async(username) => {
//         return {
//           password: 'something'
//         }
//       },
//       comparePassword: async(password, password2) => {
//         expect(password).to.equal('empty')
//         return true;
//       }
//     }
//     await login('test', 'empty', User);
//   })
//   it('Calls userDependency.comparePassword with proper user.password', async() => {
//     const User = {
//       getByUsername: async(username) => {
//         return {
//           password: 'something'
//         }
//       },
//       comparePassword: async(password, password2) => {
//         expect(password2).to.equal('something');
//         return true;
//       }
//     }
//     await login('test', 'empty', User);
//   })
// })
