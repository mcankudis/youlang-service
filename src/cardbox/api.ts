"use strict";
import * as express from 'express';
const router = express.Router();
import ensureAuthenticated from '../functions/ensureAuthenticated';
import { Cardbox } from './model';
import { Req, Res } from "../types";
import sanitize from '../functions/sanitize';

router.get('/:id', ensureAuthenticated, async(req: Req, res: Res) => {
  if(!sanitize.verifyWord(req.params.id)) return res.status(400).send("Invalid data");
  // verifyIdFunction????
  try {
    let cardbox = await Cardbox.getById(req.params.id);
    if(!cardbox) throw {code: 404, msg: "Cardbox not found"};
    // words from ids here
    res.json(cardbox);
  }
  catch(err) {
    console.error(`[${new Date}, GET /cardbox/:id]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})
router.post('/', ensureAuthenticated, async(req: Req, res: Res) => {
  if(!sanitize.verifyWord(req.body.name)) return res.status(400).send("Invalid data");
  try {
    let newCardbox = new Cardbox({
      name: req.body.name,
      createdBy: req.auth.id
    });
    let box = await newCardbox.save();
    if(!box) throw {code: 500, msg: "Unable to create cardbox"};
    res.status(201).json(box);

  }
  catch(err) {
    console.error(`[${new Date}, POST /cardbox/]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})

export default router;
