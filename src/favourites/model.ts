"use strict";
import db from '../db';

export type FavouritesDocument = db.Document & {
  user_id: string;
  list: Array<string>; // array containing the ids of words
  createdAt: string;
  updatedAt: string;
  getByUserId: getByIdFunction;
  addWord: seterFunction;
  removeWord: seterFunction;
  create: createFunction;
}

type getByIdFunction = (userId: string) => Promise<FavouritesDocument>;
type seterFunction = (wordId: string, userId: string) => Promise<boolean>;
type createFunction = (userId: string) => Promise<FavouritesDocument>;

const addToFav: seterFunction = async(wordId, userId) => {
  try {
    let res = Favourites.updateOne({user_id: userId}, {$push: {list: wordId}});
    console.log(res);
    return true;
  }
  catch(err) {
    console.error('[favourites/model/addToFav]', err);
    throw err;
  }
}
const removeFromFav: seterFunction = async(wordId, userId) => {
  try {
    let res = Favourites.updateOne({user_id: userId}, {$pull: {list: wordId}});
    console.log(res);
    return true;
  }
  catch(err) {
    console.error('[favourites/model/removeFromFav]', err);
    throw err;
  }
}
const create: createFunction = async(userId) => {
  try {
    let newFavs = new Favourites({user_id: userId});
    let favs = await newFavs.save();
    console.log(favs);
    return favs;
  }
  catch(err) {
    console.error('[favourites/model/create]', err);
    throw err;
  }
}
const getByUserId: getByIdFunction = async(id) => {
	try {
		const favs = await Favourites.findOne({user_id: id});
    if(!favs) throw {code: 404, msg: "List not found"};
    return favs;
	}
	catch(err) {
		console.error(`[${new Date()}, favourites/model/getByUserId]`, err);
		if(err.msg) throw err;
		throw {code: 500, msg: "An error occured"};
	}
}

const schema = new db.Schema({
  user_id:  {type: String, required: true, index: true, unique: true},
  list:     {type: Array, default: []},
}, {timestamps: true});

const favs = db.model<FavouritesDocument>("Favourites", schema);
export const Favourites = Object.assign(favs, {
  addWord: addToFav,
  removeWord: removeFromFav,
  getByUserId: getByUserId
})
