"use strict";
import * as express from 'express';
const router = express.Router();
import ensureAuthenticated from '../functions/ensureAuthenticated';
import { Topic } from './model';
import { Group } from '../group/model';
import { Word } from '../word/model';
import { Req, Res } from "../types";
import sanitize from '../functions/sanitize';

router.post('/', ensureAuthenticated, async(req: Req, res: Res) => {
  if(!sanitize.verifyWord(req.body.groupId)) return res.status(400).send("Invalid data");
  if(!sanitize.verifyWord(req.body.unitId)) return res.status(400).send("Invalid data");
  if(!sanitize.verifyString(req.body.name)) return res.status(400).send("Invalid data");
  if(!sanitize.verifyString(req.body.number)) return res.status(400).send("Invalid data");
  if(!sanitize.verifyString(req.body.description)) return res.status(400).send("Invalid data");
  try {
    let group = await Group.getById(req.body.groupId);
    if(!group) throw {code: 404, msg: "Group not found"};
    if(group.users.indexOf(req.auth.id)===-1)
      throw {code: 404, msg: "User not in the group"};
    let newTopic = new Topic({
      name: req.body.name,
      description: req.body.description,
      number: req.body.number,
      unit: req.body.unitId,
      group: group._id,
      createdBy: req.auth.id
    });
    newTopic.save((err, topic) => {
      if(err) throw err;
      console.log(topic);
      return res.status(201).json(topic);
    })
  }
  catch(err) {
    console.error(`[${new Date}, POST /topic/]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})

router.patch('/:id', ensureAuthenticated, async(req: Req, res: Res) => {
  try {
    if(!sanitize.verifyWord(req.params.id)) throw {code: 400, msg: "Invalid data"};
    if(!sanitize.verifyString(req.body.name)) throw {code: 400, msg: "Invalid data"};
    if(!sanitize.verifyString(req.body.description)) throw {code: 400, msg: "Invalid data"};
	  let topic = await Topic.getById(req.params.id);
	  if(!topic) throw {code: 404, msg: "Topic not found"};
    let group = await Group.getById(topic.group);
    if(!group) throw {code: 404, msg: "Group not found"};
    if(group.users.indexOf(req.auth.id)===-1)
      throw {code: 404, msg: "User not in the group"};
    Topic.findOneAndUpdate({_id: topic._id},
    {$set:{"name": req.body.name, "description": req.body.description}}, (err, topic) => {
      if(err) return res.sendStatus(500);
      console.log(err);
      console.log(topic);
      res.json(topic);
    })
  }
  catch(err) {
    console.error(`[${new Date}, PATCH /topic/]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})

router.get('/:id/words/', ensureAuthenticated, async(req: Req, res: Res) => {
  try {
    if(!sanitize.verifyWord(req.params.id)) throw {code: 400, msg: "Invalid data"};
    let topic = await Topic.getById(req.params.id);
    console.log(topic)
    let group = await Group.getById(topic.group);
    if(group.users.indexOf(req.auth.id)==-1) throw {code: 404, msg: "User not in the group"};
    let words = await Word.getByTopicId(topic._id);
    res.json(words);
  }
  catch(err) {
    console.error(`[${new Date}, GET /topic/:id/words]`, err);
    if(err.msg) return res.status(err.code).send(err.msg);
    res.status(500).send("Unknown error");
  }
})


export default router;
