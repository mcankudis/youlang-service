"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const db_1 = require("../db");
const addAdmin = async (groupId, userId) => {
    try {
        let result = await exports.Group.updateOne({ _id: groupId }, { $addToSet: { admins: [userId] } });
        console.log(`${new Date}, group/model/addAdmin`, result);
        if (result.nModified === 0)
            throw { code: 404, msg: "Group not found" };
        return { success: true };
    }
    catch (err) {
        console.error(`[${new Date}, group/model/addAdmin]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const addUser = async (groupId, userId) => {
    try {
        let result = await exports.Group.updateOne({ _id: groupId }, { $addToSet: { users: [userId] } });
        console.log(`${new Date}, group/model/addUser`, result);
        if (result.nModified === 0)
            throw { code: 404, msg: "Group not found" };
        return { success: true };
    }
    catch (err) {
        console.error(`[${new Date}, group/model/addUser]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const deleteGroup = async (groupId) => {
    try {
        let result = await exports.Group.deleteOne({ _id: groupId });
        console.log(`${new Date}, group/model/deleteGroup`, result);
        if (result.ok === 0)
            throw { code: 404, msg: "Group not found" };
        return { success: true };
    }
    catch (err) {
        console.error(`[${new Date}, group/model/deleteGroup]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const getById = async (id) => {
    try {
        const group = await exports.Group.findOne({ _id: id });
        if (!group)
            throw { code: 404, msg: "Group not found" };
        return group;
    }
    catch (err) {
        console.error(`[${new Date()}, group/model/getById]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const getByUserId = async (id) => {
    try {
        const group = await exports.Group.find({ users: id });
        return group;
    }
    catch (err) {
        console.error(`${new Date()}, [group/model/getByUserId]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const removeAdmin = async (groupId, userId) => {
    try {
        let result = await exports.Group.updateOne({ _id: groupId }, { $pull: { admins: userId } });
        console.log(`${new Date}, group/model/removeAdmin`, result);
        if (result.nModified === 0)
            throw { code: 404, msg: "Group not found" };
        return { success: true };
    }
    catch (err) {
        console.error(`[${new Date}, group/model/removeAdmin]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const removeUser = async (groupId, userId) => {
    try {
        let result = await exports.Group.updateOne({ _id: groupId }, { $pull: { users: userId } });
        console.log(`${new Date}, group/model/removeUser`, result);
        if (result.nModified === 0)
            throw { code: 404, msg: "Group not found" };
        return { success: true };
    }
    catch (err) {
        console.error(`[${new Date}, group/model/removeUser]`, err);
        if (err.msg)
            throw err;
        throw { code: 500, msg: "An error occured" };
    }
};
const groupSchema = new db_1.default.Schema({
    name: { type: String, required: true },
    description: { type: String },
    users: { type: Array, required: true },
    admins: { type: Array, required: true },
    createdBy: { type: String, required: true },
}, { timestamps: true });
const group = db_1.default.model("Group", groupSchema);
exports.Group = Object.assign(group, {
    addAdmin: addAdmin,
    addUser: addUser,
    getById: getById,
    getByUserId: getByUserId,
    removeUser: removeUser,
    removeAdmin: removeAdmin,
    delete: deleteGroup
});
