"use strict";
import db from '../db';

export type UserDocument = db.Document & {
	username: string;
  settings: object;
  createdAt: string;
  updatedAt: string;
  add: addUserFunction;
  getById: getByIdFunction;
  getByIdOrCreate: addUserFunction;
};

type getByIdFunction = (id: string) => Promise<UserDocument>;
type addUserFunction = (id: string, username: string) => Promise<UserDocument>;

const addUser: addUserFunction = async(id, username) => {
  try {
    let newUser = new YouLangUser({
      _id: id,
      username: username,
      settings: {},
      favoriteWords: []
    });
    const user = await newUser.save();
    return user;
  }
  catch(err) {
    console.error('[models/user/addUser]', err);
    throw err;
  }
}

const getById: getByIdFunction = async(id) => {
	try {
		const user = await YouLangUser.findOne({_id: id});
    if(!user) throw {code: 404, msg: "User not found"};
    return user;
	}
	catch(err) {
		console.error(`[${new Date()}, user/model/getById]`, err);
		if(err.msg) throw err;
		throw {code: 500, msg: "An error occured"};
	}
}

const getByIdOrCreate: addUserFunction = async(id, username) => {
	try {
		let user = await YouLangUser.findOne({_id: id});
    if(!user) user = await YouLangUser.add(id, username);
    return user;
	}
	catch(err) {
		console.error(`[${new Date()}, user/model/getByIdOrCreate]`, err);
		if(err.msg) throw err;
		throw {code: 500, msg: "An error occured"};
	}
}

const userSchema = new db.Schema({
  username:       {type: String, required: true},
  favoriteWords:  {type: Array},
  settings:       {type: Object}
},{timestamps: true});

const user = db.model<UserDocument>("User", userSchema);
export const YouLangUser = Object.assign(user, {
  add: addUser,
  getById: getById,
  getByIdOrCreate: getByIdOrCreate
})
